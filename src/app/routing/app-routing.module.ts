import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { CategoryComponent } from "../pages/category/category.component";
import { HomeComponent } from "../pages/home/home.component";
import { LoginComponent } from "../pages/login/login.component";
import { RegisterComponent } from "../pages/register/register.component";
import { VariantComponent } from "../pages/variant/variant.component";

const route : Routes = [
    {path:'', redirectTo:'login',pathMatch: 'full'},
    {path: 'login', component: LoginComponent},
    {path: 'home', component: HomeComponent},
    {path: 'register', component: RegisterComponent},
    {path: 'category', component: CategoryComponent},
    {path: 'variant', component: VariantComponent}
];

@NgModule({
    imports: [
        RouterModule.forRoot(route,{
            initialNavigation: 'enabled'
        }),
    ],
    exports: [RouterModule],
})
export class AppRoutingModule{}