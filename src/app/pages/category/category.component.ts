import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Category } from 'src/app/model/category';
import { CategoryService } from 'src/app/service/category.service';

@Component({
  selector: 'app-category',
  templateUrl: './category.component.html',
  styleUrls: ['./category.component.css']
})
export class CategoryComponent implements OnInit {

  public category: Category[] = [];
  public editCategory: Category;
  public deleteCategory: Category;
  constructor(private categoryService: CategoryService) { 
    this.editCategory={} as Category;
    this.deleteCategory={} as Category;
  }

  ngOnInit(): void {
    this.getCategory();
  }

  public getCategory(): void{
    this.categoryService.getCategory().subscribe(
      (response: Category[]) => {
        this.category = response;
      },
      (error: HttpErrorResponse) => {
        alert(error.message);
      }
    );
  }

  public onAddCategory(addForm: NgForm): void{
    document.getElementById('');
    this.categoryService.addCategory(addForm.value).subscribe(
      (response: Category) => {
        console.log(response);
        this.getCategory();
        addForm.reset();
      },
      (erorr: HttpErrorResponse) =>{
        alert(erorr.message);
        addForm.reset();
      }
    );
  }

  public onEditCategory(editForm: Category): void{
    this.categoryService.editCategory(editForm).subscribe(
      (response: Category) => {
        console.log(response);
        this.getCategory();
      },
      (erorr: HttpErrorResponse) =>{
        console.log(erorr.message);
        alert(erorr.message);
      }
    );
  }
  public onDeleteCategory(id: number): void{
    this.categoryService.deleteCategory(id).subscribe(
      (response: void) => {
        console.log(response);
        this.getCategory();
      },
      (erorr: HttpErrorResponse) =>{
        console.log(erorr.message);
        alert(erorr.message);
      }
    );
  }

  public onOpenModal(category: Category, mode: string): void{
    const container = document.getElementById('main-container');
    const button = document.createElement('button');
    button.type = 'button';
    button.style.display ='none';
    button.setAttribute('data-toggle', 'modal');

    if(mode=== 'add'){
      console.log('button')
      button.setAttribute('data-target', '#addCategoryModal');
    }
    if(mode=== 'edit'){
      this.editCategory= category;
      button.setAttribute('data-target', '#editCategoryModal');
    }
    if(mode=== 'delete'){
      this.deleteCategory= category;
      button.setAttribute('data-target', '#deleteCategoryModal');
    }
    container!.appendChild(button);
    button.click();
  }
  
}
