import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/auth.service';
import { User } from 'src/app/model/user';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  public user: User = new User();
  constructor(private authService: AuthService, private router: Router) { }

  ngOnInit(): void {
  }

  login(){
    this.authService.login(this.user).subscribe(
      (data : any) => {
        console.log(data);
        this.router.navigate(['home']);
      },
      (err) => {
        console.log(err.error);
      },
      () => console.log('Login Successfully')
    )
  }
  moveToRegister(){
    this.router.navigate(['register']);
  }
}
