import { Component, OnInit } from '@angular/core';
import { Register } from 'src/app/model/reg';
import { AuthService } from 'src/app/auth.service';
import { Router} from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  public register: Register = new Register();
  constructor(private authService: AuthService, private router: Router) { }
  

  ngOnInit(): void {
  }
  authRegister(){
    this.authService.register(this.register).subscribe(
      (data : any) => {
        console.log(data);
        this.router.navigate(['login']);
      },
      (err) => {
        console.log(err.error);
      },
      () => console.log('Login Successfully')
    )
  }
}
