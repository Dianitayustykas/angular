export interface Variant{
    id : number;
    variantCode: string;
    variantName: string;
}