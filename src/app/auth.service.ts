import { HttpClient, HttpClientModule } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Config } from './base/config';
import {User} from './model/user';
import {Register} from './model/reg';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private http: HttpClient) { }

  public login(user: User){
    return this.http.post(Config.url + 'api/auth/signin', user);
  }
  public register(reg: Register){
    return this.http.post(Config.url + 'api/auth/signup' , reg);
  }
  
}
