import { HttpClient, HttpHeaders } from '@angular/common/http';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Config } from '../base/config';
import { Variant } from '../model/variant';
// Variant''
@Injectable({
  providedIn: 'root'
})
export class VariantService {

  constructor(private http: HttpClient) { }

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type' : 'application/json',
      'Access-Control-Allow-Origin' : '*',
      Authorization: 'Bearer eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJuaXRvcyIsImlhdCI6MTYyNTIxMTI2Mn0.06yE5TEOvwt-_s9kVzTEeEb7vh1rsA0a-3R1farIWX8Eali6vyr8BZTrzj-xwaF5pWaCWT8sl2P5_ut_734cBQ'
    })
  }

  public getVariant(): Observable<Variant[]> {
    return this.http.get<Variant[]> (
      `${Config.url}api/variant`,
      this.httpOptions
    )
  }

  public addVariant(variant: Variant): Observable<Variant> {
      return this.http.post<Variant>(
      Config.url + 'api/variant',
      variant, {
        ...this.httpOptions,
        responseType: 'text' as 'json',
      }
    )
  }

  public editVariant(variant: Variant): Observable<Variant> {
    return this.http.put<Variant>(
    Config.url + 'api/variant',
    variant, {
      ...this.httpOptions,
      responseType: 'text' as 'json',
    }
  )
  }

  public deleteVariant(id: number): Observable<void> {
    return this.http.delete<void>(
    Config.url + `api/variant/${id}`, {
      ...this.httpOptions,
      responseType: 'text' as 'json',
    }
  )
}
}